import osproc
import strutils

import common

proc stripQuotes(str: string): string =
  var text = replace(str,"\"","&quot;")
  return text

proc quote*(str: string): string =
  var text = str
  # May need to put some further work to escape some special chars here
  text = stripQuotes(text)
  
  # Put leading and ending quote marks in
  return " \"" & text & "\" "
  #       ^  Add a spaces  ^ so the previous flag isn't touching
  
proc markup(str: string): string =
  # Placeholder proc for future use
  var text = stripQuotes(str)
  return text

proc copyToClipboard*(str: string): bool {.discardable.} =
  let ok = execCmd("echo -n " & quote(str) & " | xclip -selection clipboard")
  return ok == 0

proc listify(data:Info, opts: varargs[string]): string =
  var text = data.full_text
  if opts.len > 0:
    text &= "\n"
    for opt in opts:
      text &= markup text
      text &= "\n"
  return text

proc genMenuCmd(data: Info, opts: varargs[string]): string =
  var cmd = ""
  # length of the list of opts, plus the full text
  var x_lines = len(opts) + 1
  # if the text is empty, we don't want to create a menu item of it
  if data.full_text != "":
    let text = markup data.full_text
    cmd &= text & "\n"
  else:
    x_lines -= 1
  for opt in opts:
    let text = markup opt
    cmd &= text & "\n"
  
  cmd.removeSuffix("\n")
  
  if x_lines > myConfig.max_lines: x_lines = myConfig.max_lines

  if myConfig.prepend:
    cmd = "echo -e" & quote(cmd) & "| "
    cmd &= myConfig.exec
    cmd &= " -i" # set case insensitive
    cmd &= " -p" & quote(data.title)
    cmd &= "-l " & $x_lines
  echo "Sending command:\n" & cmd
  return cmd

proc runExec(data: Info, opts: varargs[string]): string =
  if not myConfig.to_stdout:
    let cmd = genMenuCmd(data, opts)
    var output = execCmdEx(cmd)
    echo "Output:\n" & $output
    output.output.stripLineEnd()
    return output.output
  else:
    stdout.writeLine listify(data,opts)

proc outputData*(data: Info, args: varargs[string,`$`]): string {.discardable.} =
  var output = runExec(data,args)
  return output
