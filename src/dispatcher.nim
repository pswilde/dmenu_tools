
import common
import util/furrytime
import util/pingclock
import util/batturry
import util/volurrme
import util/netwurrk
import util/wirelurrs
import util/emurrji
import util/calendurr
import util/remminurr
import util/passwurrd
import util/pw_generaturr
import util/temperaturr
import util/screenshurrt
import util/calculaturr
import util/brightnurrs
import util/tideurrl
import util/wallpapurr

proc dispatch*(cfg: Config) =
  case cfg.run
  of FurryTime:
    furrytime.go()
  of PingClock:
    pingclock.go()
  of Batturry:
    batturry.go()
  of Volurrme:
    volurrme.go()
  of Netwurrk:
    netwurrk.go()
  of Wirelurrs:
    wirelurrs.go()
  of Emurrji:
    emurrji.go()
  of Calendurr:
    calendurr.go()
  of Remminurr:
    remminurr.go()
  of Passwurrd:
    passwurrd.go()
  of PasswurrdGeneraturr:
    pw_generaturr.go()
  of Temperaturr:
    temperaturr.go()
  of Screenshurrt:
    screenshurrt.go()
  of Calculaturr:
    calculaturr.go()
  of Brightnurrs:
    brightnurrs.go()
  of Tideurrl:
    tideurrl.go()
  of Wallpapurr:
    wallpapurr.go()
  else:
    echo "No valid run command given"
