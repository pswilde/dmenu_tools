import osproc
import re
import strutils

import ../common
import ../output

const host: string = "9.9.9.9"

let ping_re = re(r"time=[0-9.]+")
const ping_cmd: string = "ping -4 -c 1 " & host

proc getPing(): float =
  var ping: float = -1
  let cmdOut = execCmdEx(ping_cmd)
  let lines = splitLines(cmdOut.output)
  let ping_line = lines[1]
  let bounds = findBounds(ping_line, ping_re)
  if bounds.first > 0:
    let png = ping_line[bounds.first+5..bounds.last]
    ping = parseFloat(png)
  return ping

proc getObject(ping: float): Info =
  let pingstr = split($ping,".")
  let niceping = pingstr[0] & "." & pingstr[1][0]
  var text = "🏓 " & niceping & " ms"
  var state = 0
  if ping < 0:
    text = "❌ No Pong"
    state = 1
  else:
    case ping:
      of 0..100:
        state = 0
      of 101..400:
        state = 1
      of 401..1000:
        state = 2
      else:
        state = 9

  var data = newInfo("Ping Clurrk")
  data.full_text = text
  # i3bar stuff
  return data


proc go*() =
 let ping = get_ping()
 let data = getObject(ping)
 let output = outputData(data)
 if output == data.full_text:
   go()

