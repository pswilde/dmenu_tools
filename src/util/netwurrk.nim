import os
import osproc
import strutils
import sequtils

import ../common
import ../output

const mng_cmd = "alacritty -e nmtui-connect" 

proc getIP(nic: string): string =
  let cmd = "ifconfig " & nic & " | grep inet | awk -F\" \" '{print $2}' | head -1 | awk '{print $1}'" 
  let ip = execCmdEx(cmd)
  return strip(ip.output)

proc getOnlineState(nic: string): string =
  try:
    let oper = readFile("/sys/class/net/" & nic & "/operstate")
    let state = strip(oper)
    return "[" & state & "]"
  except:
    echo "Error getting operstate for " & nic & " : ", getCurrentExceptionMsg()
    return ""

proc getConnState(nic: string): (string, string) =
  let state = getOnlineState(nic)
  let ip = getIP(nic)
  if state == "[down]" or ip == "":
    return ("disconnected", state)
  return (ip, state)

proc getObject(): Info =
  var data = newInfo("Netwurrk")
  return data

proc getNetInfo*(my_nics: seq[string])  =
  var my_nic_states: seq[string] = @[]
  for nic in my_nics:
    let (ip, state) = getConnState(nic)
    my_nic_states.add(nic & ":" & state & " " & ip)
  let data = getObject()
  let args = concat(my_nic_states,@["manage"])
  let option = outputData(data, args)
  if option in my_nic_states:
    discard execCmd(mng_cmd)
  case option:
    of "manage":
      discard execCmd(mng_cmd)

proc getNics*(): seq[string] =
  var my_nics: seq[string] = @[]
  for nic in os.walkDir("/sys/class/net/"):
    let n = replace(nic.path, "/sys/class/net/", "")
    my_nics.add(n)

  if len(my_nics) > 0:
    return my_nics
  return @["no-nic"]

proc go*() =
  getNetInfo(getNics())

