
import httpclient
import json
import strutils
import random

import ../parser
import ../model/pwgen
import ../common
import ../output

var passwords: seq[string]
proc getNumber(size: int = 4): string =
  var num = ""
  for _ in countup(1,size):
    randomize()
    let roll = rand(0..9)
    num &= $roll
  return num

proc parsePasswords(body: string, digits: int) =
  passwords = @[]
  let j = body.parseJson
  for pass in j.getElems:
    var p = pass.getStr.capitalizeAscii
    p &= getNumber(digits)
    passwords.add(p)
  
proc getPasswords(pwgen: PWGen) =
  var c = newHttpClient()
  try:
    let url = "https://random-word-api.herokuapp.com/word?number=" & $pwgen.number & "&length=" & $pwgen.word_len
    let resp = c.get(url)
    if resp.status == $Http200:
      parsePasswords(resp.body, pwgen.digits)
  except:
    stderr.writeLine getCurrentExceptionMsg()

proc getOutput(): Info =
  var data = newInfo("PW Generaturr")
  data.full_text = "Refresh"
  return data

proc goOutput(args: PWGen) =
  let data = getoutput()
  let selected = outputData(data,passwords)
  if selected in passwords:
    copyToClipboard(selected)
  elif selected == "Refresh":
    getPasswords(args)
    goOutput(args)

proc go*() =
  echo "Getting passwords..."
  let args = parsePWGenArgs()
  getPasswords(args)
  if args.to_terminal:
    for pw in passwords:
      echo pw
  else:
    echo "Outputting to app"
    goOutput(args)

if isMainModule:
  go()
