import os
import osproc
import tables
import algorithm

import configparser

import ../common
import ../output

const REMMINA_DIR = getHomeDir() & ".local/share/remmina"

var sessions = initTable[string,string]()
var names: seq[string] = @[]

proc getRemminaFiles(): seq[string] =
  if len(names) < 1:
    for file in walkFiles(REMMINA_DIR & "/*.remmina"):
      let content = readFile(file)
      let ini = parseIni(content)
      let group = ini.getProperty("remmina","group")
      let name = ini.getProperty("remmina","name")
      let server = ini.getProperty("remmina","server")
      if name != "" and server != "":
        let slug = group & " : " & name & " : (" & server & ")"
        sessions[slug] = file
        names.add(slug)
    names.sort()
  return names

proc editRemmina(conn: string) =
  let session = sessions[conn]
  discard execCmd("remmina -e " & quote(session))

proc startRemmina(conn: string) =
  let session = sessions[conn]
  discard execCmd("remmina -c " & quote(session))

proc go*() # adding as need to refer back to it in selectRemmina

proc selectRemmina(conn: string) =
  var info = newInfo("Remmina Choosurr : " & conn)
  let args = @["connect", "edit", "back"]
  let output = outputData(info,args)
  if output in args:
    case output:
      of "connect":
        startRemmina(conn)
        #switchWorkspace()
      of "edit":
        editRemmina(conn)
        #switchWorkspace()
      of "back":
        go()

proc go*() =
  var info = newInfo("Remmina Choosurr")
  var args: seq[string] = getRemminaFiles()
  args.add("new")
  args.add("exit")
  let output = outputData(info,args)
  if output == "exit" or output == "":
    return
  elif output == "new":
    discard execCmd("remmina --new")
  elif output in names:
    selectRemmina(output)
    return
