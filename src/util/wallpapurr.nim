import os
import json
import osproc
import random
import strutils
import sequtils
import httpclient

import ../common
import ../common/http
import ../common/display
import ../parser
import ../output
import ../notify

var UNSPLASH_KEY = ""
var BG_DIR = "/tmp/"
var LAST_FILE = ""
var LAST = ""

const UNSPLASH_URL = "https://api.unsplash.com/photos/random?query=$QUERY&orientation=landscape"

proc getFromUnsplash(q: var string): string = 
  let dir = BG_DIR & "/unsplash/" & q & "/"
  createDir(dir)
  notify.send("Getting from Unsplash",q)
  q = q.replace(" ","%20")
  let uri = UNSPLASH_URL.replace("$QUERY",q)
  var client = newHttpClient(timeout = 10000)
  let id = "Client-ID " & UNSPLASH_KEY
  client.headers = newHttpHeaders({"Authorization": id})
  try:
    let resp = client.get(uri)
    if resp.status == $Http200:
      let j = parseJson(resp.body)
      let link = j["links"]["download"].getStr
      let filename = dir & j["slug"].getStr & ".jpg"
      let img = download(link)
      filename.save(img)
      return filename
  except:
    echo getCurrentExceptionMsg()

proc getFiles(dir: string): seq[string] =
  var files: seq[string] = @[]
  for file in walkDir(dir):
    if file.path.endsWith(".jpg"): files.add(file.path)
    elif file.kind == pcDir:
      files = files.concat(getFiles(file.path))
  return files

proc getLast() =
  LAST = readFile(LAST_FILE).strip()

proc setLastFileName(file: string) =
  writeFile(LAST_FILE, file)
  LAST = file

proc getImageFromDir(): string =
  notify.send("Getting Random file from:",BG_DIR)
  var img_files = getFiles(BG_DIR).filter(proc(f: string): bool = f != LAST)
  randomize()
  img_files.shuffle()
  let img_file = img_files[0]
  notify.send("Found : ", img_file)
  return img_file

proc getCurrSwayBGPID(): string =
  let pid = execCmdEx("pgrep swaybg")
  return pid.output

proc killCurrSwayBGPID(pid: string) =
  sleep 2000
  discard execCmd("kill " & pid)


proc setImage(img: string) =
  notify.send("Setting Background to:",img)
  if isWayland():
    let pid = getCurrSwayBGPID()
    let swaybg = "swaybg -m fill -i " & img.escape & " &"
    discard execCmd(swaybg)
    killCurrSwayBGPID(pid)
  else:
    let feh = "feh --bg-fill " & img.escape
    discard execCmdEx(feh)

proc setLast() =
  notify.send("Setting Background to Last", LAST)
  if isWayland():
    let pid = getCurrSwayBGPID()
    let swaybg = "swaybg -m fill -i " & LAST.escape & " &"
    discard execCmd(swaybg)
    killCurrSwayBGPID(pid)
  else:
    let feh = "feh --bg-fill " & LAST.escape
    discard execCmdEx(feh)

proc getDesign(): Info =
  var data = newInfo("Wallpapurr")
  return data

proc queryPrompt(): string =
  let data = getDesign()
  let output = data.outputData()
  return output

proc go*() =
  var args = parseWallpapurrArgs()
  UNSPLASH_KEY = myConfig.unsplash_key
  BG_DIR = myConfig.bg_dir
  LAST_FILE = BG_DIR & "/last.txt"

  if args.from_unsplash and args.query == "":
    args.query = queryPrompt()

  var img = ""
  if args.query != "" or args.from_unsplash:
    echo "Query: ", args.query
    img = getFromUnsplash(args.query)
  elif args.last:
    getLast()
    setLast()
    quit(0)
  else:
    img = getImageFromDir()
  setImage(img)
  setLastFilename(img)
  echo img
  




