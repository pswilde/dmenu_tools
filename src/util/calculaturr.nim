import osproc
import strutils
import sequtils

import ../common
import ../output

# using qalc as it has a lot of nice inbuilt features
# may be nice to make it totally non-dependant on other things though

const exitSeq = @["---","exit"]
proc doCalculation(calc: string) =
  var info = newInfo("Calculaturr")
  let ans_cmd_terse = "echo \"" & calc & "\" | qalc +u8 -terse -color=never | awk '!/^>/ && !/^$/ {gsub(/^[ \\t]+|[ \\t]+$/, \"\", $0); print}'"
  let ans_cmd_full = "echo \"" & calc & "\" | qalc +u8 -color=never | awk '!/^>/ && !/^$/ {gsub(/^[ \\t]+|[ \\t]+$/, \"\", $0); print}'"
  var ans_full = execCmdEx(ans_cmd_full)
  ans_full.output.stripLineEnd()
  var ans_terse = execCmdEx(ans_cmd_terse)
  ans_terse.output.stripLineEnd()
  let answers = @[strip(ans_full.output),
                  strip(ans_terse.output)]
  let args = concat(answers,exitSeq)
  var cmd = outputData(info, args)
  if cmd in answers:
    copyToClipboard(cmd)
  elif cmd in exitSeq or cmd == "":
    return
  else:
    doCalculation(cmd)

proc go*() =
  var info = newInfo("Calculaturr")
  let args = @["exit"]
  let cmd = outputData(info, args)
  if cmd != "":
    doCalculation(cmd)
  return

