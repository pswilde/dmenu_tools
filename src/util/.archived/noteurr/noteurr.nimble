# Package

version       = "0.1.0"
author        = "Paul Wilde"
description   = "A notes app for dmenu"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["noteurr"]


# Dependencies

requires "nim >= 1.6.6"
