# Package

version       = "0.1.0"
author        = "Paul Wilde"
description   = "Query libretranslate with dmenu"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["translaturr"]


# Dependencies

requires "nim >= 1.6.6"
