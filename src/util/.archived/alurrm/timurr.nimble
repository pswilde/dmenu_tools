# Package

version       = "0.1.0"
author        = "Paul Wilde"
description   = "A timer/alarm tool"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["timurr"]


# Dependencies

requires "nim >= 1.6.6"
requires "argparse"
