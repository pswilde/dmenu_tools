import ../../globurrl
import argparse

const DURATION_FMT="HH:mm:ss"

var parser = newParser:
  flag "-s", "--show", help="Show current timers"
  flag "-a", "--add", help="Add a new timer, requires -d,--duration"
  option "-d", "--duration", help="Duration in HH:mm:ss format", default=some("")

proc show_timurr() =
  echo "Hello"

when isMainModule:
  echo("This is still a work in progress")
  try:
    var opts = parser.parse()
    if opts.show:
      show_timurr()
    elif opts.add:
      if opts.duration != "":
        echo "hi"
      else:
        stderr.writeLine "Cannot add (-a) without durations (-d)"
  except ShortCircuit as e:
    if e.flag == "argparse_help":
      echo parser.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)
