import std/[json,os,marshal,osproc,re]
import std/[asyncdispatch, httpclient]
import ../../globurrl

type
  Bookmark = object
    name: string
    shortname: string
    url: string

const TITLE = "Burrkmarks"
let title_re = re("<title>(.*?)<\\/title>", {reMultiLine})
let title_rem_re = re("<\\/?title>")
let https_re = re("https?:\\/\\/")
let bookmarks_file = getSyncDir() & "bookmarks.json"
var bookmarks: seq[Bookmark] = @[]

proc `$`(bookmark: Bookmark): string =
  let show_name = if bookmark.shortname != "": bookmark.shortname else: bookmark.name
  let x = show_name & " - [" & bookmark.url & "]"
  return x

proc getTitle(bookmark: Bookmark): Future[string] {.async.} =
  var client = newAsyncHttpClient()
  try:
    let html = await client.getContent(bookmark.url)
    let titles = html.findAll(title_re)
    if len(titles) > 0:
      var title = titles[0]
      title = title.replace(title_rem_re,"")
      return title
  except:
    echo getCurrentExceptionMsg()
    return ""

proc save(bookmarks: seq[Bookmark]): bool {.discardable.} =
  let data = pretty(%*bookmarks)
  writeFile(bookmarks_file, data)

proc get(bookmarks: seq[Bookmark], str: string): Bookmark =
  for bookmark in bookmarks:
    if str == $bookmark:
      return bookmark
  return Bookmark()

proc checkFile(): bool =
  if not fileExists(bookmarks_file):
    writeFile(bookmarks_file,"")
  if fileExists(bookmarks_file):
    return true
  return false
  
proc getBookmarks(): seq[Bookmark] =
  let f = open(bookmarks_file)
  try:
    let nodes = parseJson(f.readAll())
    for node in nodes:
      var bookmark = Bookmark() 
      bookmark.name = node.getOrDefault("name").getStr()
      bookmark.shortname = node.getOrDefault("shortname").getStr()
      bookmark.url = node.getOrDefault("url").getStr()
      bookmarks.add(bookmark)
  except:
    echo getCurrentExceptionMsg()
  return bookmarks

proc addBookmark(link: string) =
  var url = link
  var bookmark = Bookmark()
  if not url.contains(https_re):
    url = "https://" & url
  bookmark.url = url
  bookmark.name = waitFor bookmark.getTitle()
  bookmarks.add(bookmark)
  bookmarks.save()

proc goToBookmark(bookmark: string) = 
  let bm = bookmarks.get(bookmark)
  discard execCmd("xdg-open " & bm.url)

proc toStrList(bookmarks: seq[Bookmark]): seq[string] =
  var list: seq[string] = @[]
  for bookmark in bookmarks:
    list.add($bookmark)
  return list

proc showBookmarks() =
  let info = newInfo(TITLE)
  let args = bookmarks.toStrList()
  let option = outputData(info, args)
  if option == "":
    echo "Empty input, closing..."
    return
  elif option notin args:
    echo "Adding bookmark: ", option
    addBookmark(option)
    showBookmarks()
  elif option in args:
    echo "Opening bookmark: ", option
    goToBookmark(option)

proc start() =
  if checkfile():
    bookmarks = getBookmarks()
    showBookmarks()
  else:
    echo "File : ", bookmarks_file, " does not exist."

when isMainModule:
  start()
