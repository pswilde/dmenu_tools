import os
import strutils
import sequtils
import osproc

import ../common
import ../output
import ../parser
import ../model/volume

const audio_tools = @["ncpamixer", "pavucontrol"]
const vol_cmd = "pamixer"
const vol_up = vol_cmd & " -i %v" # where %v is amount by
const vol_down = vol_cmd & " -d %v" # where %v is amount by
const vol_set = vol_cmd & " --set-volume %v" # where %v is amount by
const vol_mute = vol_cmd & " -t"
const vol_default_by = "5"
const vol_get = vol_cmd & " --get-volume"
const vol_get_mute = vol_cmd & " --get-mute"


proc getCurrentVolume(): string =
  let mute = execCmdEx(vol_get_mute)
  if strip(mute.output) == "true":
    return "muted"
  let vol = execCmdEx(vol_get)
  return vol.output

proc checkVolume(volume: string): string =
  var vol = volume
  if strip(vol) == "Connection error":
    sleep(1000)
    vol = getCurrentVolume()
    vol = checkVolume(vol)
  return vol

proc getDesign(volume: string): string =
  let vol = checkVolume(volume)
  var icon = " "
  if vol == "muted":
    return icon & "muted"
  let pcnt = parseInt(strip(vol))
  case pcnt:
    of 85..100:
      icon = " "
    of 55..84:
      icon = " "
    of 35..54:
      icon = " "
    of 10..34:
      icon = " "
    else:
      icon = " "
  let main_text = icon & $pcnt & "%"
  return main_text
  
proc volumeUp() =
  let cmd = replace(vol_up, "%v", vol_default_by)
  discard execCmd(cmd)

proc volumeDown() =
  let cmd = replace(vol_down, "%v", vol_default_by)
  discard execCmd(cmd)

proc volumeMute() =
  discard execCmd(vol_mute)

proc getVolume*() =
  let vol = getCurrentVolume()
  let main_text = getDesign(vol)
  var data = newInfo("Volurrme")
  data.full_text = main_text
  let args = concat(@["up", "down", "mute", "---", "exit", "---"],audio_tools)
  let option = outputData(data,args)
  if option == "":
    return
  elif option in args:
    if option in audio_tools:
      discard(execCmd(option))
      return
    case option:
      of "up":
        volumeUp()
        getVolume()
      of "down":
        volumeDown()
        getVolume()
      of "mute":
        volumeMute()
        getVolume()
      of "---":
        getVolume()
      of "exit":
        return
  else:
    try:
      let vol = parseInt(option)
      let cmd = replace(vol_set, "%v", $vol)
      discard execCmd(cmd)
      getVolume()
    except:
      echo getCurrentExceptionMsg()
      getVolume()

proc go*() =
  let arg = parseVolArgs()
  case arg
  of VolUp:
    volumeUp()
  of VolDown:
    volumeDown()
  of VolMute:
    volumeMute()
  else:
    getVolume()
