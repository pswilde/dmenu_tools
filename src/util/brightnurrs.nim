import os
import strutils
import osproc
import math

import ../common
import ../parser
import ../model/brightness
import ../output

#const backlight = "intel_backlight"
const BACKLIGHT_CMD = "xbacklight"
const UP = BACKLIGHT_CMD & " -inc %v" # %v is amount by
const DOWN = BACKLIGHT_CMD & " -dec %v" # %v is amount by
const SET = BACKLIGHT_CMD & " -set %v" # %v is amount by
const default_value = "5"

proc getBacklight(): string = 
  for dir in walkDir("/sys/class/backlight"):
    echo dir.path
    var bl = dir.path.replace("/sys/class/backlight/","")
    echo bl
    return bl

proc getLimit(backlight: string): int =
  try:
    if backlight != "":
      let back_l = readFile("/sys/class/backlight/" & backlight & "/max_brightness")
      return parseInt(strip(back_l))
  except:
    echo "Error getting backlight max : ", getCurrentExceptionMsg()
  return -1


proc getDesign(pcnt: float): string =
  var icon = "🌑"
  case pcnt:
    of 85..100:
      icon = "🌕"
    of 75..85:
      icon = "🌖"
    of 50..75:
      icon = "🌗"
    of 35..50:
      icon = "🌘"
    else:
      icon = "🌑"
  let percent = toInt(round(pcnt,0))
  let text = icon & " " & $percent & "%"
  return text

proc brightnessUp() =
  let cmd = replace(UP,"%v",default_value)
  discard execCmd(cmd)
proc brightnessDown() =
  let cmd = replace(DOWN,"%v",default_value)
  discard execCmd(cmd)

proc getBrightness*(backlight: string) =
  var data = newInfo("Brightnurrs")
  if backlight == "":
    data.full_text = "No Backlight Found"
    discard outputData(data)
    quit(1)
  let limit = getLimit(backlight)
  let current = parseInt(strip(readFile("/sys/class/backlight/" & backlight & "/actual_brightness")))
  let pcnt = (current/limit)*100
  let text = getDesign(pcnt)
  data.full_text = text
  let args = @["up", "down"]
  let option = outputData(data,args)
  if option in args:
    case option:
      of "up":
        brightnessUp()
        backlight.getBrightness()
      of "down":
        brightnessDown()
        backlight.getBrightness()
  else:
    try:
      let i = parseInt(option)
      let cmd = replace(SET,"%v",$i)
      discard execCmd(cmd)
      backlight.getBrightness()
    except:
      echo getCurrentExceptionMsg()

proc go*() =
  let backlight = getBacklight()
  let barg = parseBrightnessArgs()
  case barg:
    of BrightUp:
      brightnessUp()
    of BrightDown:
      brightnessDown()
    else:
      backlight.getBrightness()
