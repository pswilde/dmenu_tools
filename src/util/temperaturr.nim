import os
import re
import math
import strutils

import ../common
import ../output

proc getThermalZones(): seq[string] =
  var zones: seq[string] = @[]
  let dirname_re = re("thermal_zone[\\d]+")
  let enabled_re = re("enabled")
  for file in walkDir("/sys/class/thermal/"):
    if file.path.contains(dirname_re):
      let state = readFile(file.path & "/mode")
      if state.contains(enabled_re):
        zones.add(file.path)
  return zones

proc getTemp(zone: string): int =
  let temp = strip(readFile(zone & "/temp"))
  return parseInt(temp)

proc getAverageTemp(zones: seq[string]): float =
  var temps: int = 0
  for zone in zones:
    let temp = getTemp(zone)
    temps += temp
  let avgtemp = ceil((temps / len(zones))/1000)
  return round(avgtemp,2)

proc getObject(temp: float): Info =
  var icon = ""
  case temp:
    of 40..59:
      icon = ""
    of 60.. 200:
      icon = ""
    else:
      icon = ""
  let main_text = icon & " " & $temp & "°C"
  var data = newInfo("Temperaturr")
  data.full_text = main_text
  return data

proc go*() =
  let zones = getThermalZones()
  let temp = getAverageTemp(zones)
  let data = getObject(temp)
  let option = outputData(data)
  if option == data.full_text:
    # Refresh
    go()

