import times
import os
import osproc
import strutils
import sequtils

import ../common
import ../common/display
import ../output
import ../parser
import ../model/screenshot

const DATE_FORMAT = "yyyyMMdd-hhmmss"
const FILENAME = "Screenshot-%d.png"
const TEMP_DIR = "/tmp/"
let DATE_STR = now().format(DATE_FORMAT)

let CLIPBOARD_CMD = if isWayland(): WL_CLIPBOARD_CMD else: X_CLIPBOARD_CMD

proc saveToClipboard(filename: string) =
  let cmd = "cat " & filename & " | " & CLIPBOARD_CMD
  let status = execCmd(cmd)
  if status == 0 and fileExists(filename):
    removeFile(filename)
  return

proc saveToFile(filename: string) =
  if fileExists(filename):
    let new_filename = filename.replace("/tmp/", getHomeDir() & "Screenshots/")
    copyFile(filename, new_filename)
    if fileExists(new_filename):
      removeFile(filename)
  return

proc openFile(filename: string) =
  let cmd = "xdg-open " & filename
  discard execCmd(cmd)
  return

proc showScreenshotSaveSel(filename: string) =
  let info = newInfo("Screenshurrt")
  let args = @["clipboard", "save", "open", "---", "exit"]
  let choice = outputData(info,args)
  if choice == "---":
    showScreenshotSaveSel(filename)
  elif choice == "exit":
    return
  elif choice in args:
    case choice:
      of "clipboard":
        saveToClipboard(filename)
      of "save":
        saveToFile(filename)
      of "open":
        openFile(filename)
  return

proc showScreenshotSizeSel(): ScreenshotSize =
  let info = newInfo("Screenshurrt type")
  let args = concat(ScreenshotSizes(),@["---","exit"])
  let choice = outputData(info,args)
  if choice.isScreenshotSize():
    return choice.toScreenshotSize()
  elif choice == "---":
    return showScreenshotSizeSel()
  elif choice == "exit":
    quit(0)
  else:
    quit(0)

proc takeScreenshot(ss: Screenshot) =
  let filename = TEMP_DIR & FILENAME.replace("%d",DATE_STR)
  var cmd = ss.tool.command
  case ss.size:
    of Window:
      cmd = ss.tool.activeWindowCommand()
    of Region:
      cmd = ss.tool.regionCommand()
    else: #fullscreen
      cmd = cmd.replace("%s","")
      # sleep for a bit otherwise the screen shot could grabs dmenu as well
      sleep(1*500)
  cmd = cmd.replace("%f",filename)
  echo "Running command:\n" & cmd
  let status = execCmd(cmd)
  if status == 0:
    showScreenshotSaveSel(filename)
  return

proc go*() =
  var ss  = parseScreenshotArgs()
  if ss.size == None:
    ss.size = showScreenshotSizeSel()
  if ss.size != None:
    ss.takeScreenshot()

if isMainModule:
  go()
