import ../lib/emurrjilist
import ../common
import ../output

import re

proc go*() =
  var info = newInfo("Emurrji")
  var args = getEmoji()
  args.add("exit")
  let output = outputData(info,args)
  if output == "exit" or output == "":
    return
  else:
    let e = re.findAll(output,re(".+ :"))
    if len(e) > 0:
      let emoji = re.replace(e[0], re(" :"),"")
      copyToClipboard(emoji)
  return

