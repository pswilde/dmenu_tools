import std/envvars

const XDG_SESSION_TYPE = "XDG_SESSION_TYPE" 
const WAYLAND = "wayland"

proc isWayland*(): bool =
  if existsEnv(XDG_SESSION_TYPE) and getEnv(XDG_SESSION_TYPE) == WAYLAND:
    return true
  return false
