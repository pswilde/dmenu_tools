import httpclient

proc download*(link: string): string =
  var client = newHttpClient(timeout = 10000)
  try:
    let resp = client.get(link)
    if resp.status == $Http200:
      return resp.body
  except:
    echo getCurrentExceptionMsg()
  return ""

proc save*(file: string, content: string) =
  writeFile(file,content)
