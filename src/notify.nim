import strutils
import osproc

type
  Note* = object
    urgency*: Urgency
    title*: string
    content*: string
    timeout*: int
  Urgency* = enum
    Normal = "normal"
    Low = "low"
    Urgent = "urgent"
    Critical = "critical"

proc newNote*(): Note =
  return Note(urgency: Normal, title: "Notification", content: "Hello, I am a notifications", timeout: 2000)

proc `$`*(n: Note): string =
  let str = "notify-send -u $U $T $C -t $N"
           .replace("$U", $n.urgency)
           .replace("$T", n.title.escape)
           .replace("$C", n.content.escape)
           .replace("$N", $n.timeout)
  return str

proc send*(n: Note) =
  discard execCmdEx($n)

proc send*(s: varargs[string,`$`]) = 
  var n = newNote()
  n.title = s[0]
  if s.len > 1:
    n.content = s[1..^1].join("")
  else:
    n.content = ""
  send(n)

