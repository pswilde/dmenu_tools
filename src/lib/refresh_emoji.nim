import jsony
import strutils
import re
import httpclient
import emurrjilist

var emojis: seq[Emoji] = @[]
let e_re = re"E[0-9\.]+"
const group_line = "# group: "
const subgroup_line = "# subgroup: "

proc parse(body: string) =
  var current_group = ""
  var current_subgroup = ""
  for line in body.split("\n"):
    if line.startsWith(group_line):
      let g = line.replace(group_line, "")
      if current_group != g:
        current_group = g
      continue
    if line.startsWith(subgroup_line):
      let sub_g = line.replace(subgroup_line, "")
      if current_subgroup != sub_g:
        current_subgroup = sub_g
      continue
    if line == "" or (line.len > 0 and line[0] == '#'):
      continue
    try:
      let parts = line.split("#")
      let emo = parts[1].split(e_re)
      var emoji = Emoji()
      emoji.emoji = emo[0].strip(chars={' '})
      emoji.name = emo[1].strip()
      emoji.group = current_group
      emoji.subgroup = current_subgroup
      emojis.add(emoji)
    except:
      echo getCurrentExceptionMsg()

proc getUnicodeOrgEmoji() =
  const url = "https://unicode.org/Public/emoji/latest/emoji-test.txt"
  var client = newHttpClient()
  let page = client.get(url)
  parse(page.body)

proc save(emojis: seq[Emoji]) =
  let file = "emojis.json"
  writeFile(file, emojis.toJson())

proc getEmojis() =
  getUnicodeOrgEmoji()
  save(emojis)

when isMainModule:
  getEmojis()
