import jsony

type
  Emoji* = object
    emoji*: string
    name*: string
    group*: string
    subgroup*: string

proc `$`(e: Emoji): string = 
  return e.emoji & " : " & e.name & " : " & e.group & " : " & e.subgroup

const include_groups = ["Smileys & Emotion"]
#, "People & Body"]
#, "Animals & Nature"]
#, "Food & Drink"]
#, "Travel & Places", "Activities", "Objects"]
const ignore_subgroups = ["religion"]

proc getEmojis(): seq[string] =
  let file_emojis = staticRead("../../emojis.json")
  let emojis = file_emojis.fromJson(seq[Emoji])
  var list: seq[string] = @[]
  for e in emojis:
    if e.group in include_groups and e.subgroup notin ignore_subgroups:
      list.add($e)
  return list

const emoji_list = getEmojis()

proc getEmoji*(): seq[string] =
  return emoji_list

