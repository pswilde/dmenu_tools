
type
  Tool* = enum
    None,
    FurryTime,
    PingClock,
    Batturry,
    Volurrme,
    Netwurrk,
    Wirelurrs,
    Emurrji,
    Calendurr,
    Remminurr,
    Passwurrd,
    PasswurrdGeneraturr,
    Temperaturr,
    Screenshurrt,
    Calculaturr,
    Brightnurrs,
    Tideurrl,
    Wallpapurr
