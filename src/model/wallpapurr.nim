
type
  WPArgs* = object
    query*: string
    last*: bool
    from_unsplash*: bool
