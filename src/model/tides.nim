import times

const TIDE_URL* = "https://www.tidetimes.org.uk/%LOC-tide-times"
const DEFAULT_LOC* = "exmouth-dock"

type 
  Tide* = ref object
    state*: string
    time*: string
    height*: string
    tomorrow*: bool
  TideList* = ref object
    tides*: seq[Tide]
    url*: string
    last_updated*: DateTime
    location*: string

proc newTideList*(): TideList =
  var tl = TideList()
  tl.url = TIDE_URL
  tl.location = DEFAULT_LOC
  return tl
