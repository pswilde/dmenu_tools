
type
  BrightnessArg* = enum
    None,
    BrightUp,
    BrightDown
