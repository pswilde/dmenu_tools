import os
import strutils
import parsetoml

import tool
import screenshot

type
  Config* = ref object
    exec*: string
    run*: Tool
    max_lines*: int
    prepend*: bool
    to_stdout*: bool
    screenshot_tool*: ScreenshotTool
    unsplash_key*: string
    bg_dir*: string

let config_dir* = getHomeDir() & ".config/wm_tools/"
let config_file* = config_dir & "config.toml"

proc `$`(c: Config): string =
  var str = "exec = \"" & c.exec & "\"\n"
  str &= "prepend = " & $c.prepend & "\n"
  str &= "screenshot_tool = \"" & $c.screenshot_tool & "\"\n"
  str &= "max_lines = " & $c.max_lines
  str &= "\n"
  return str

proc newConfig*(): Config =
  var cfg = Config()
  cfg.exec = "rofi -dmenu"
  cfg.prepend = true
  cfg.screenshot_tool = Maim
  cfg.max_lines = 20

  discard existsOrCreateDir(config_dir)
  if not fileExists(config_file):
    writeFile(config_file,$cfg)
  else:
    let content = readFile(config_file)
    try:
      let toml = parseString(content)
      if toml.hasKey("exec"):
        cfg.exec = toml["exec"].getStr
      if toml.hasKey("max_lines"):
        cfg.max_lines = toml["max_lines"].getInt
      if toml.hasKey("screenshot_tool"):
        cfg.screenshot_tool = toml["screenshot_tool"].getStr.toScreenshotTool
      if toml.hasKey("unsplash_key"):
        cfg.unsplash_key = toml["unsplash_key"].getStr
      if toml.hasKey("bg_dir"):
        cfg.bg_dir = toml["bg_dir"].getStr.replace("$HOME",getHomeDir())
    except:
      echo "Error with Config File:"
      echo getCurrentExceptionMsg()
  return cfg
