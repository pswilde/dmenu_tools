
import sequtils
import strutils

type
  Screenshot* = object
    size*: ScreenshotSize
    tool*: ScreenshotTool
  ScreenshotSize* = enum
    None = ""
    Region = "region",
    Full = "fullscreen",
    Window = "window"
  ScreenshotTool* = enum
    None = ""
    Maim = "maim"
    Grim = "grim"

proc newScreenshot*(): Screenshot =
  var ss = Screenshot()
  ss.tool = Maim
  ss.size = None
  return ss

proc toScreenshotSize*(str: string): ScreenshotSize =
  case str
  of "region": return Region
  of "fullscreen": return Full
  of "window": return Window
  else: return None

proc isScreenshotSize*(str: string): bool =
  return str.toScreenshotSize != None

proc ScreenshotSizes*(): seq[string] =
  var sizes: seq[string] = @[]
  for item in ScreenshotSize.toSeq:
    if item != None:
      sizes.add($item)
  return sizes

proc toScreenshotTool*(str: string): ScreenshotTool =
  case str
  of "maim": return Maim
  of "grim": return Grim
  else: return None

proc isScreenshotTool*(str: string): bool =
  return str.toScreenshotTool != None

proc command*(tool: ScreenshotTool): string =
  case tool
  of Maim: return "maim -u %s --format png %f"
  of Grim: return "grim %s %f"
  else: return ""

proc activeWindowCommand*(tool: ScreenshotTool): string =
  var cmd = tool.command()
  # where %s is an extra flag or process, i.e. xdotool for getting active window
  case tool
  of Maim:
    cmd = cmd.replace("%s","-i $(xdotool getactivewindow)")
  of Grim:
    echo "Not currently Implemented"
    quit(1)
  else: return cmd
  return cmd

proc regionCommand*(tool: ScreenshotTool): string =
  var cmd = tool.command()
  case tool
  of Maim:
    cmd = cmd.replace("%s","-s")
  of Grim:
    cmd = cmd.replace("%s","-g \"$(slurp)\"")
  else: return cmd
  return cmd

const X_CLIPBOARD_CMD* = "xclip -selection clipboard -t image/png"
const WL_CLIPBOARD_CMD* = "wl-copy"

