# Package

version       = "2.0.7"
author        = "Paul Wilde"
description   = "A set of informational tools"
license       = "AGPL-3.0-or-later"
srcDir        = "src"
bin           = @["wmtools"]


# Dependencies

requires "nim >= 2.0.0"
requires "parsetoml >= 0.7.1"
requires "argparse"
requires "configparser"
requires "jsony"

task refresh_emoji, "Refresh Emoji Library file":
  exec "nim c -r src/lib/refresh_emoji.nim"
